package com.oc.game;

import java.util.Scanner;

public class AmitieGarde {

    static class Main {

        static Scanner sc = new Scanner(System.in);

        public static void main(String[] args) {
            int gardeDebut1 = sc.nextInt();
            int gardeFin1 = sc.nextInt();
            int gardeDebut2 = sc.nextInt();
            int gardeFin2 = sc.nextInt();

            if  ((gardeDebut1 > gardeFin2) || (gardeDebut2 > gardeFin1) ) {
                System.out.println("Pas amis");
            } else {
                System.out.println("Amis");
            }
        }
    }
}
