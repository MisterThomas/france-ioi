package com.oc.game;

import java.util.Scanner;

public class Caserne {
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int rectangle = sc.nextInt();

        for (int loop = 1; loop <= rectangle; loop = loop + 1)
        {
            int xMin1 =sc.nextInt();
            int xMax1 =sc.nextInt();
            int yMin1 =sc.nextInt() ;
            int yMax1 = sc.nextInt();
            int xMin2 =sc.nextInt();
            int xMax2 =sc.nextInt();
            int yMin2 =sc.nextInt();
            int yMax2=sc.nextInt();

            if((xMax2 <= xMin1) || (xMax1 <= xMin2)|| (yMax1 <= yMin2) || (yMax2 <= yMin1)) {
                System.out.println("NON");
            } else {
                System.out.println("OUI");
            }
        }
    }
}
