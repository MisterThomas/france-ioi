package com.oc.game;

import java.util.Scanner;

public class Chimie {
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int nb = sc.nextInt();
        int  min = sc.nextInt();
        int  max = sc.nextInt();
        int total = 0;
        boolean ras = true;
        while(total <  nb && ras){
            int temp = sc.nextInt();
            ras =  (total < nb && min <= temp &&  temp <= max);
            if(ras){
                System.out.println("Rien à signaler");
            } else {
                System.out.println("Alerte !!");
            }
            total++;
        }
    }
}
