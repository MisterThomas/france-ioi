package com.oc.game;

import java.util.Scanner;

public class Choixemplacements {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int nbMarchands = sc.nextInt();
        int tab[] = new int[nbMarchands];
        for (int participant = 0; participant < nbMarchands; participant++){
            int marchand = sc.nextInt();
            tab[marchand] = participant;
        }
        for (int numero = 0; numero < nbMarchands; numero++){
            System.out.println(tab[numero]);
        }

    }
}
