package com.oc.game;

import java.util.Arrays;
import java.util.Scanner;

public class CourseTroisPieds
{


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int nbCoureur = sc.nextInt();

        int [] tab = new  int [nbCoureur];
        for(int loop = 0; loop < nbCoureur; loop++){
            tab [loop] =sc.nextInt();
        }
        Arrays.sort(tab);

        for (int loop = 0; loop < nbCoureur/2;loop++){
            System.out.println(tab[loop] + " " + tab[nbCoureur - loop -1]);
        }
    }
}
