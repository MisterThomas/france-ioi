package com.oc.game;

import java.util.Scanner;

public class Etudemarche {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);


        int nbProduits = sc.nextInt();
        int nbPersonnes = sc.nextInt();

        int[] tab = new int[nbProduits];

        for(int i = 0; i < nbProduits; i++){
            tab[i] = 0;
        }

        for(int j = 0; j < nbPersonnes; j++){
            int numeroProduit =sc.nextInt();
            tab[numeroProduit]++;
        }

        for(int k = 0; k < nbProduits; k++){
            System.out.println(tab[k]);
        }
    }
}
