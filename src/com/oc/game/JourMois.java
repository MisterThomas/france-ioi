package com.oc.game;

import java.util.Scanner;

public class JourMois {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int mois = sc.nextInt();

        int jour = 0;

        for(int loop = 1; loop <= mois; loop++) {

            if((mois == 1) || (mois == 2)  || (mois == 3) || (mois == 7) || (mois == 8) || (mois == 9)) {
                jour = 30;
            }

            if((mois == 4) || (mois == 5)  || (mois == 6) || (mois == 10)) {
                jour = 31;
            }

            if(mois == 11) {
                jour = 29;
            }

        }
        System.out.println(jour);
    }
}
