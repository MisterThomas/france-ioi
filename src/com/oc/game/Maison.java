package com.oc.game;

import java.util.Scanner;

public class Maison {
    static class Main {
        static Scanner sc = new Scanner(System.in);

        public static void main(String[] args) {

            int abscisseMin = sc.nextInt();
            int abscisseMax = sc.nextInt();
            int ordonnéeMin = sc.nextInt();
            int ordonnéeMax = sc.nextInt();

            int maison = sc.nextInt();

            int suspect = 0;

            for( int loop = 1; loop <= maison; loop++){
                int  abscisseMaison = sc.nextInt();
                int   ordonneeMaison = sc.nextInt();

                if((abscisseMaison >= abscisseMin)&&(abscisseMax >=abscisseMaison)&&(ordonneeMaison >=          ordonnéeMin)&&(ordonneeMaison <=ordonnéeMax)){
                    suspect = suspect + 1;
                }

            }

            System.out.println(suspect);
        }
    }
}
