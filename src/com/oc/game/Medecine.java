package com.oc.game;

import java.util.Scanner;

public class Medecine {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int population = sc.nextInt();

        int malade = 1;
        int jour = 1;

        while(malade < population) {

            jour = jour + 1;
            malade = malade * 3;
        }
        System.out.println(jour);
    }
}
