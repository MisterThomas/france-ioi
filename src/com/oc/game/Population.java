package com.oc.game;

import java.util.Scanner;
import static java.lang.Math.*;

public class Population {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int habitant1 = sc.nextInt();
        double croissance1 = sc.nextDouble();
        double croissance1sur100 = croissance1 / 100;
        double ecartCroissance1 = habitant1 * croissance1sur100;
        double ecart1 = floor(ecartCroissance1);

        int habitant2 = sc.nextInt();
        double croissance2 = sc.nextDouble();
        double croissance2sur100 = croissance2 / 100;
        double ecartCroissance2 = habitant2 * croissance2sur100;
        double ecart2 = floor(ecartCroissance2);

        System.out.println(habitant1 + (int)ecart1);
        System.out.println(habitant2 +(int) ecart2);
    }
}
