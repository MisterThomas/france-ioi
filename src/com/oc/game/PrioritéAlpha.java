package com.oc.game;

import java.util.Scanner;

public class PrioritéAlpha {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String auteur1 = sc.nextLine();
        String auteur2 = sc.nextLine();
        if (auteur1.compareTo(auteur2) < 0)
        {
            System.out.println(auteur1);
        }
        if (auteur1.compareTo(auteur2) == 0)
        {
            System.out.println();
        }
        if (auteur1.compareTo(auteur2) > 0)
        {
            System.out.println(auteur2);
        }
    }
}
