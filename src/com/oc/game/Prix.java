package com.oc.game;

import java.util.Scanner;

public class Prix {
    static class Main {
        static Scanner sc = new Scanner(System.in);
        public static void main(String[] args) {
            int nbMarchands = sc.nextInt();

            int prixMax = 1000000;
            int prixMin = 0;

            for (int loop = 1; loop <= nbMarchands; loop++){
                int prix = sc.nextInt();

                if (prix <= prixMax){
                    prixMax = prix;
                    prixMin = loop;
                }
            }
            System.out.println(prixMin);
        }
    }
}
