package com.oc.game;

import java.util.Scanner;

public class RepartitionPoids {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {


        int nbCharrette = sc.nextInt();

        double[] tab = new double[nbCharrette];

        double moyenne = 0.00;


        for (int loop = 0; loop < nbCharrette; loop++){
            double poidsCharrette = sc.nextDouble();
            tab[loop] = poidsCharrette;
            moyenne += tab[loop];
        }

        moyenne = moyenne/nbCharrette;

        for (int loop = 0; loop < nbCharrette; loop++){
            System.out.println(moyenne-tab[loop]);
        }
    }
}
