package com.oc.game;

import java.util.Scanner;

public class ResumerLivres {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int nblignes = sc.nextInt();
        int longueur = sc.nextInt();

        for (int texte = 1; texte <= nblignes; texte++) {
            String auteur = sc.nextLine();
            String phrase = sc.nextLine();

            if (phrase.length() < longueur) {
                System.out.println(auteur);
            }
        }
    }
}
