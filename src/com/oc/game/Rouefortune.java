package com.oc.game;

import java.util.Scanner;

public class Rouefortune {

        static Scanner sc = new Scanner(System.in);

        public static void main(String[] args) {

            int nbcase = sc.nextInt();

            int roue = 24;

            int resultat = nbcase % roue;

            if (resultat < 0){
                resultat = resultat + 24;
            }

            System.out.println(resultat);
        }
}
