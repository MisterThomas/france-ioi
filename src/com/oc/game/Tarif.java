package com.oc.game;

import java.util.Scanner;

public class Tarif {
    static class Main {
        static Scanner sc = new Scanner(System.in);
        public static void main(String[] args) {

            int age = sc.nextInt();
            int kiloValise = sc.nextInt();
            int ecuAge = 0;
            int ecuValise = 0;
            int total = 0;

            if (age == 60) {
                ecuAge = 0;
            } else if (age <= 10) {
                ecuAge = 5;
            } else if (age > 10 ) {
                ecuAge = 30;
            }

            if (kiloValise >= 20) {
                ecuValise = 10;
            }

            total = ecuValise + ecuAge;
            System.out.println(total);
        }
    }
}
