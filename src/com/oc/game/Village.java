package com.oc.game;

import java.util.Scanner;

public class Village {
    static class Main {
        static Scanner sc = new Scanner(System.in);
        public static void main(String[] args) {
            int tour = sc.nextInt();
            int minX = 1000000;
            int minY = 1000000;
            int maxX = 0;
            int maxY = 0;

            for( int loop = 0; loop < tour; loop++){
                int abscisseX = sc.nextInt();
                int ordonneeY = sc.nextInt();

                if(abscisseX  < minX) {
                    minX = abscisseX;
                }

                if(abscisseX  > maxX) {
                    maxX = abscisseX;
                }

                if(ordonneeY > maxY) {
                    maxY = ordonneeY;
                }

                if(ordonneeY < minY) {
                    minY = ordonneeY;
                }
            }
            int total = ((maxX - minX)*2) + ((maxY - minY)*2);
            System.out.println(total);
        }
    }
}
