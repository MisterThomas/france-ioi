package com.oc.game;

import java.util.Scanner;

public class Visitemine {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int nbDeplacement = sc.nextInt();

        int[] tab = new int[nbDeplacement];

        for(int i = 0; i < nbDeplacement; i++){
            int deplacement = sc.nextInt();
            tab[i] += deplacement ;
        }

        for(int i = nbDeplacement - 1; i >= 0 ; i--){
            if(tab[i] == 5){
                System.out.println("4");
            } else if(tab[i] == 4) {
                System.out.println("5");
            }else if(tab[i] == 2){
                System.out.println("1");
            } else if(tab[i] == 1){
                System.out.println("2");
            }
            else {
                System.out.println(tab[i]);
            }

        }

    }
}
